import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBnGjKU7LKsBK2NMphpOwqGM6AbYzIvCyY",
    authDomain: "enact-a7e19.firebaseapp.com",
    projectId: "enact-a7e19",
    storageBucket: "enact-a7e19.appspot.com",
    messagingSenderId: "483091959850",
    appId: "1:483091959850:web:8261b65cea65ae67b13353",
    measurementId: "G-DVHXPXPWHV"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider }
export default db;